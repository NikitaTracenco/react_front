import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Schedule from "@material-ui/icons/Schedule";
import List from "@material-ui/icons/List";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import NavPills from "components/NavPills/NavPills.js";

import styles from "assets/jss/material-kit-react/views/componentsSections/pillsStyle.js";



import InputAdornment from "@material-ui/core/InputAdornment";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Radio from "@material-ui/core/Radio";
import Switch from "@material-ui/core/Switch";

import Favorite from "@material-ui/icons/Favorite";
import People from "@material-ui/icons/People";
import Check from "@material-ui/icons/Check";
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";


import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomLinearProgress from "components/CustomLinearProgress/CustomLinearProgress.js";
import Paginations from "components/Pagination/Pagination.js";
import Badge from "components/Badge/Badge.js";


import { Apps, CloudDownload } from "@material-ui/icons";
import ListItem from "@material-ui/core/ListItem";
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import { Link } from "react-router-dom";


import Clicker from "components/ClickerColor/ClickerColor.js";
//images for format Design
import modal from "assets/img/modal.jpg";
import slide from "assets/img/slide.jpg";
import fixed from "assets/img/fixed.jpg";
import noneim from "assets/img/None.jpg";
import topim from "assets/img/Top.jpg";
import rightim from "assets/img/Right.jpg";
import leftim from "assets/img/Left.jpg";





const useStyles = makeStyles(styles);

export default function SectionPills() {
  const classes = useStyles();
  const [checked, setChecked] = React.useState([24, 22]);
  const [selectedEnabled, setSelectedEnabled] = React.useState("b");
  const [checkedA, setCheckedA] = React.useState(true);
  const [checkedB, setCheckedB] = React.useState(false);
  const [checkedC, setCheckedC] = React.useState(false);

  const handleToggle = value => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
  };

  return (
    <div className={classes.section}>
      <div className={classes.container}>
        <div id="navigation-pills">
          <div className={classes.title}>
            <h3>Navigation Pills</h3>
          </div>
          <div className={classes.title}>
            <h3>
              <small>With Icons</small>
            </h3>
          </div>
          <GridContainer>
            <GridItem xs={12} sm={12} md={8} lg={6}>
              <NavPills
                color="primary"
                tabs={[
                  {
                    tabButton: "Design",
                    tabIcon: Dashboard,
                    tabContent: (
                        <div className={classes.container}>
                          <div id="buttons">
                            <div className={classes.title}>
                              <h3>
                                Format
                                <br />
                                <small>Pick your style</small>
                              </h3>
                            </div>
                            <GridContainer justify="center">
                              <GridItem xs={12} sm={12} md={8}>
                                <Button color="primary" round>
                                <div>
                                  <img src={modal} weight="70px" height="70px" alt="First slide"/>
                                  <br />Modal
                                </div>

                                </Button>

                                <Button color="primary" round>
                                <div>
                                  <img src={slide} weight="70px" height="70px" alt="Second slide"/>
                                  <br />Slide
                                </div>
                                </Button>

                                <Button color="primary" round>
                                <div>
                                  <img src={fixed} weight="70px" height="70px" alt="Therd slide"/>
                                  <br />Fixed
                                </div>
                                </Button>

                              </GridItem>
                            </GridContainer>
                            </div>


                            <div className={classes.title}>
                              <h3>
                                Popup settings
                                <br />
                              <small>Display</small>
                              </h3>
                            </div>


                            <List className={classes.list}>
                              <ListItem className={classes.listItem}>
                                <CustomDropdown
                                  noLiPadding
                                  buttonText="Components"
                                  buttonProps={{
                                    className: classes.navLink,
                                    color: "primary"
                                  }}
                                  buttonIcon={Apps}
                                  dropdownList={[
                                    <Link to="/" className={classes.dropdownLink}>
                                      All components
                                    </Link>,
                                    <a
                                      href="https://creativetimofficial.github.io/material-kit-react/#/documentation?ref=mkr-navbar"
                                      target="_blank"
                                      className={classes.dropdownLink}
                                    >
                                      Documentation
                                    </a>
                                  ]}
                                />
                              </ListItem>
                              </List>


                              <div id="buttons">
                                <div className={classes.title}>
                                  <h3>
                                    Image alignment
                                  </h3>
                                </div>
                                <GridContainer justify="center">
                                  <GridItem xs={12} sm={12} md={8}>
                                    <Button color="primary" round>
                                    <div>
                                      <img src={noneim} weight="70px" height="70px" alt="First slide"/>
                                      <br />None
                                    </div>

                                    </Button>

                                    <Button color="primary" round>
                                    <div>
                                      <img src={topim} weight="70px" height="70px" alt="Second slide"/>
                                      <br />Top
                                    </div>
                                    </Button>

                                    <Button color="primary" round>
                                    <div>
                                      <img src={rightim} weight="70px" height="70px" alt="Therd slide"/>
                                      <br />Right
                                    </div>
                                    </Button>

                                    <Button color="primary" round>
                                    <div>
                                      <img src={leftim} weight="70px" height="70px" alt="Therd slide"/>
                                      <br />Left
                                    </div>
                                    </Button>

                                  </GridItem>
                                </GridContainer>
                                </div>


                            <div className={classes.title}>
                              <h3>
                              Field labels
                              <br />
                            <small>Font</small>
                              </h3>
                              </div>


                            <List className={classes.list}>
                              <ListItem className={classes.listItem}>
                                <CustomDropdown
                                  noLiPadding
                                  buttonText="Components"
                                  buttonProps={{
                                    className: classes.navLink,
                                    color: "primary"
                                  }}
                                  buttonIcon={Apps}
                                  dropdownList={[
                                    <Link to="/" className={classes.dropdownLink}>
                                      All components
                                    </Link>,
                                    <a
                                      href="https://creativetimofficial.github.io/material-kit-react/#/documentation?ref=mkr-navbar"
                                      target="_blank"
                                      className={classes.dropdownLink}
                                    >
                                      Documentation
                                    </a>
                                  ]}
                                />
                              </ListItem>
                              </List>

                              <div className={classes.title}>
                                <h3>
                              <small>Text colour</small>
                                </h3>
                                <Clicker />
                              </div>

                              <div className={classes.title}>
                                <h3>
                                Button Style
                                <br />
                              <small>Text</small>
                                </h3>
                                <GridContainer>
                                  <GridItem xs={12} sm={4} md={4} lg={3}>
                                    <CustomInput
                                      id="regular"
                                      inputProps={{
                                        placeholder: "Text"
                                      }}
                                      formControlProps={{
                                        fullWidth: true
                                      }}
                                    />
                                  </GridItem>
                                  </GridContainer>
                                  <h3>
                                  <small>Text color</small>
                                  </h3>
                                  <Clicker />

                                  <h3>
                                  <small>Background color</small>
                                  </h3>
                                  <Clicker />

                                  <h3>
                                  <small>Hover color</small>
                                  </h3>
                                  <Clicker />

                                  <h3>
                                  <small>alignment</small>
                                  </h3>
                                  <List className={classes.list}>
                                    <ListItem className={classes.listItem}>
                                      <CustomDropdown
                                        noLiPadding
                                        buttonText="Components"
                                        buttonProps={{
                                          className: classes.navLink,
                                          color: "primary"
                                        }}
                                        buttonIcon={Apps}
                                        dropdownList={[
                                          <Link to="/" className={classes.dropdownLink}>
                                            All components
                                          </Link>,
                                          <a
                                            href="https://creativetimofficial.github.io/material-kit-react/#/documentation?ref=mkr-navbar"
                                            target="_blank"
                                            className={classes.dropdownLink}
                                          >
                                            Documentation
                                          </a>
                                        ]}
                                      />
                                    </ListItem>
                                    </List>

                                    <GridContainer>
                                      <GridItem xs={12} sm={6} md={4} lg={3}>
                                        <div className={classes.title}>
                                          <h3>Checkboxes</h3>
                                        </div>
                                        <div
                                          className={
                                            classes.checkboxAndRadio +
                                            " " +
                                            classes.checkboxAndRadioHorizontal
                                          }
                                        >

                                          <FormControlLabel
                                            control={
                                              <Checkbox
                                                tabIndex={-1}
                                                onClick={() => handleToggle(21)}
                                                checkedIcon={<Check className={classes.checkedIcon} />}
                                                icon={<Check className={classes.uncheckedIcon} />}
                                                classes={{
                                                  checked: classes.checked,
                                                  root: classes.checkRoot
                                                }}
                                              />
                                            }
                                            classes={{ label: classes.label, root: classes.labelRoot }}
                                            label="Unchecked"
                                          />
                                        </div>




                                        <div
                                          className={
                                            classes.checkboxAndRadio +
                                            " " +
                                            classes.checkboxAndRadioHorizontal
                                          }
                                        >
                                          <FormControlLabel
                                            control={
                                              <Checkbox
                                                tabIndex={-1}
                                                onClick={() => handleToggle(22)}
                                                checked={checked.indexOf(22) !== -1 ? true : false}
                                                checkedIcon={<Check className={classes.checkedIcon} />}
                                                icon={<Check className={classes.uncheckedIcon} />}
                                                classes={{
                                                  checked: classes.checked,
                                                  root: classes.checkRoot
                                                }}
                                              />
                                            }
                                            classes={{ label: classes.label, root: classes.labelRoot }}
                                            label="Checked"
                                          />
                                        </div>

                                        </GridItem>
                                        </GridContainer>
                              </div>

                              <div className={classes.title}>
                                <h3>
                                Popup style
                                <br />
                              <small>Overlay opacity (%)</small>
                                </h3>
                                <GridContainer>
                                  <GridItem xs={12} sm={4} md={4} lg={3}>
                                    <CustomInput
                                      id="regular"
                                      inputProps={{
                                        placeholder: "50%"
                                      }}
                                      formControlProps={{
                                        fullWidth: true
                                      }}
                                    />
                                  </GridItem>
                                  </GridContainer>
                              </div>
                                </div>
                    )
                  },
                  {
                    tabButton: "Fields",
                    tabIcon: Schedule,
                    tabContent: (
                      <div className={classes.container}>
                      <div className={classes.space70} />
                      <div id="checkRadios">
                        <GridContainer>
                          <GridItem xs={12} sm={6} md={4} lg={4}>
                          <div className={classes.title}>
                            <h3>
                              Format
                              <br />
                              <small>Name</small>
                              <br />
                              <small>Email Address</small>
                              <br />
                              <small>Birthday</small>
                            </h3>
                          </div>
                          </GridItem>



                          <GridItem xs={12} sm={6} md={4} lg={3}>
                            <div className={classes.title}>
                              <h3>Required</h3>
                            </div>
                            <div>
                              <FormControlLabel
                                control={
                                  <Switch
                                    checked={checkedA}
                                    onChange={event => setCheckedA(event.target.checked)}
                                    value="checkedA"
                                    classes={{
                                      switchBase: classes.switchBase,
                                      checked: classes.switchChecked,
                                      thumb: classes.switchIcon,
                                      track: classes.switchBar
                                    }}
                                  />
                                }
                                classes={{
                                  label: classes.label
                                }}
                                label=""
                              />
                            </div>

                            <div>
                              <FormControlLabel
                                control={
                                  <Switch
                                    checked={checkedB}
                                    onChange={event => setCheckedB(event.target.checked)}
                                    value="checkedB"
                                    classes={{
                                      switchBase: classes.switchBase,
                                      checked: classes.switchChecked,
                                      thumb: classes.switchIcon,
                                      track: classes.switchBar
                                    }}
                                  />
                                }
                                classes={{
                                  label: classes.label
                                }}
                                label=""
                              />
                            </div>

                            <div>
                              <FormControlLabel
                                control={
                                  <Switch
                                    checked={checkedC}
                                    onChange={event => setCheckedC(event.target.checked)}
                                    value="checkedC"
                                    classes={{
                                      switchBase: classes.switchBase,
                                      checked: classes.switchChecked,
                                      thumb: classes.switchIcon,
                                      track: classes.switchBar
                                    }}
                                  />
                                }
                                classes={{
                                  label: classes.label
                                }}
                                label=""
                              />
                            </div>
                          </GridItem>

                        </GridContainer>
                      </div>
                    </div>
                    )
                  },
                  {
                    tabButton: "Image",
                    tabIcon: Schedule,
                    tabContent: (
                      <div className={classes.container}>
                        <div className={classes.title}>
                          <h2>Image
                          <br />
                          <small>Suggested dimensions:</small>
                          </h2>
                        </div>
                      <GridContainer justify="center">
                      <GridItem xs={12} sm={12} md={8}>
                    <div id="blockContent">
                      <div style={{float: 'left'}}>

                        <div>
                          <img src={modal} weight="170px" height="170px" alt="First slide"/>
                        </div>

                      </div>

                      <div style={{bottom: '0'}}>
                        <Button color="primary" round>
                        <div>
                          Slide
                        </div>
                        </Button>
                        <br />
                        <Button color="primary" round>
                        <div>
                          Edit
                        </div>
                        </Button>
                      </div>
                    </div>
                        </GridItem>
                      </GridContainer>



                      <GridItem xs={12} sm={6} md={4} lg={3}>
                        <div className={classes.title}>
                          <h3>Required</h3>
                        </div>
                        <div>
                          <FormControlLabel
                            control={
                              <Switch
                                checked={checkedA}
                                onChange={event => setCheckedA(event.target.checked)}
                                value="checkedA"
                                classes={{
                                  switchBase: classes.switchBase,
                                  checked: classes.switchChecked,
                                  thumb: classes.switchIcon,
                                  track: classes.switchBar
                                }}
                              />
                            }
                            classes={{
                              label: classes.label
                            }}
                            label=""
                          />
                          Edge to edge
                        </div>
                        </GridItem>


                        <div className={classes.title}>
                          <h2>
                          <small>Body</small>
                          </h2>
                          <GridContainer>
                            <GridItem xs={12} sm={4} md={4} lg={3}>
                              <CustomInput
                                id="regular"
                                inputProps={{
                                  placeholder: "Give me some text"
                                }}
                                formControlProps={{
                                  fullWidth: true
                                }}
                              />
                            </GridItem>
                            </GridContainer>
                        </div>

                        <div className={classes.title}>
                          <h2>
                          <small>Footer</small>
                          </h2>
                          <GridContainer>
                            <GridItem xs={12} sm={4} md={4} lg={3}>
                              <CustomInput
                                id="regular"
                                inputProps={{
                                  placeholder: "Give me some text"
                                }}
                                formControlProps={{
                                  fullWidth: true
                                }}
                              />
                            </GridItem>
                            </GridContainer>
                        </div>

                        </div>


                    )
                  }
                ]}
              />
            </GridItem>

          </GridContainer>
        </div>
      </div>
    </div>
  );
}
