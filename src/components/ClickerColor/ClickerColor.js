import React from 'react';
import InputColor from 'react-input-color';

export default function ClickerColor() {
  const [color, setColor] = React.useState({});

  return (
    <div>
      <InputColor
        initialValue="#5e72e4"
        onChange={setColor}
        placement="right"
      />

    </div>
  );
}
